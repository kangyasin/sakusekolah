<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Saku Sekolah

Aplikasi saku sekolah coming soon.

## Install

- git clone https://gitlab.com/kangyasin/sakusekolah.git
- composer install
- setup file .env atau rename .env.example menjadi .env dan atur variabel database, sesuai dengan nama database yang kalian ingin gunakan
- php artisan config:cache && php artisan cache:clear
- php artisan migrate
- php artisan passport:install

## Test

Untuk menjalankan unit test pastikan proses install diatas berjalan dengan baik.

- setup .env.test
- atur koneksi database ke database yang berbeda dengan setup install (buat database baru)
- php artisan migrate --env=setting
- php artisan test
