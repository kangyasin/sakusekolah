<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Support\Facades\Log;
use Throwable;
use App\Helpers\ResponseHelper;
use Illuminate\Http\JsonResponse;

class UnhandledException extends Exception
{
    public $user;

    public $feature;

    public $previous;

    /**
     * UnhandledException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     * @param null $user
     * @param null $feature
     */
    public function __construct($message = '', $code = 0, Throwable $previous = null, $user = null, $feature = null)
    {
        $this->user = $user;

        if ($this->user == null && auth()->check()) {
            $this->user = auth()->user();
        }
        $this->feature = $feature;
        $this->previous = $previous;
        $this->message = $message;
        parent::__construct($message, $code, $previous);
    }

    /**
     * Report exception.
     */
    public function report()
    {
        if ($this->previous === null) {
            Log::alert('Unhandled Exception. Message: '.$this->message);

            return;
        }
        $messages = ' Unhandled Exception Message: '.$this->getMessage().
            ' File: '.$this->getFile().
            ' Line: '.$this->getLine().
            ' Exception Message: '.$this->previous->getMessage().
            ' Exception File: '.$this->previous->getFile().
            ' Exception Line: '.$this->previous->getLine();

        if ($this->user !== null) {
            Log::alert('User: '.$this->user->email.$messages);
        } else {
            Log::alert($messages);
        }
        //send email to monitoring
    }

    /**
     * @param $request
     * @return JsonResponse
     */
    public function render($request)
    {
        return ResponseHelper::json(false, 500, trans('response-message.unhandled'));
    }
}
