<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\UnhandledException;
use App\Helpers\ResponseHelper;
use App\Http\Controllers\ResponseController;
use App\Http\Requests\RegisterRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\ValidationException;

class AuthController extends ResponseController
{
    //
    /**
     * login api
     *
     * @return JsonResponse
     */
    public function login()
    {
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $token =  $user->createToken('sakusekolah')->accessToken;
            return response()->json([
                'error' => false,
                'message' => 'Berhasil Login',
                'token' => $token,
                'id' => $user->id,
                'email' => $user->email,
            ], 200);
        }

        return response()->json([
            'error' => true,
            'message' => 'Email atau password salah !'
        ], 200);
    }

    /**
     * Get the authenticated User.
     *
     * @responseFile status=401 scenario="api_key not provided" responses/unauthenticated.json
     * @responseFile responses/auth/me.json
     *
     * @param  \Illuminate\Http\Request  $request
     * @return JsonResponse
     */
    public function me()
    {
        $id = auth()->user()->id;
        $user = User::where('id', $id)->first();

        return response()->json($user, 200);
    }

    /**
     * Attempt to logout the user.
     *
     * After successfull logut the token get invalidated and can not be used further.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return JsonResponse
     */
    public function logout(Request $request)
    {
        try {
            $request->user()->token()->revoke();
            return $this->respond([
                'message' => 'Berhasil keluar aplikasi',
            ]);
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }


    }

    /**
     * Attempt to register the user.
     *
     * @param RegisterRequest $request
     * @return JsonResponse
     * @throws UnhandledException
     */
    public function register(RegisterRequest $request)
    {
        $check = User::where('email', $request['email']);
        if($check->count() > 0)
        {
            return $this->respond([
                'error' => true,
                'message' => 'Pendaftaran gagal email atau sekolah sudah digunakan',
            ]);
        }

        $user = User::create($request->all());
//        $user->assignRole('user');

        return $this->respond([
            'error' => false,
            'message' => 'Pendaftaran anda berhasil',
            'id' => $user->id,
            'email' => $user->email,
        ]);
    }

}
