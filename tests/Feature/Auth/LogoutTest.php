<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LogoutTest extends TestCase
{

    public function test_user_can_logout()
    {
        $this->artisan('passport:install');

        $user = User::factory()->create([
            'password' => bcrypt($password = 'demo123'),
        ]);

        $attribute = ['password' => $password, 'email' => $user->email];
        $response = $this->postJson('api/auth/login', $attribute);

        $attribute_me = ['Authorization' => 'Bearer ' . $response['token']];
        $me = $this->getJson('api/auth/logout', $attribute_me);
        $me->assertSuccessful();

    }

}
