<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class GetProfileTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_get_profile_info()
    {
        $this->artisan('passport:install');

        $user = User::factory()->create([
            'password' => bcrypt($password = 'demo123'),
        ]);

        $attribute = ['password' => $password, 'email' => $user->email];
        $response = $this->postJson('api/auth/login', $attribute);
        $attribute_me = ['Authorization' => 'Bearer ' . $response['token']];
        $me = $this->getJson('api/auth/me', $attribute_me);
        $me->assertSuccessful();

    }
}
